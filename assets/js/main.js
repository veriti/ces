var website = website || {};

website.core = (function () {

	'use strict';
	var utils = {
		isElementInView : function (element, fullyInView) {
			var pageTop = $(window).scrollTop() + 200;
			var pageBottom = pageTop + $(window).height() - 200;
			var elementTop = $(element).offset().top;
			var elementBottom = elementTop + $(element).height();

			if (fullyInView === true) {
				return ((pageTop < elementTop) && (pageBottom > elementBottom));
			} else {
				return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
			}
		}
	};
	var page = {
		init: function () {
			this.smoothScroll();
			this.playBox();
			this.vsBall();
			this.sectionVisibility();

            var vsTesti = new Swiper('.vs-testi-swiper', {
                autoplay: 11000,
                speed: 800,
                effect: 'slide',
                loop: true,
				autoHeight: true
            });

            var count = (function () {
                var value; var slides;
                if($(window).width() < 767) {
                    value = 1;
                    slides = 2;
                } else {
                    value = 2;
                    slides = 4;
                }
                return {
                    slidesPerColumnVal: value,
                    slidesPerView: slides
                };
            })();
            var gallery =  new Swiper('.vs-gallery', {
                slidesPerView: count.slidesPerView,
                autoplayDisableOnInteraction: false,
                spaceBetween: 0,
                loop: true,
                autoplay: 1,
                speed: 8000,
				followFinger: false,
				shortSwipes: false,
				longSwipes: false,
				freeModeMomentumBounce: false,
				freeMode: true,
				freeModeMomentumBounceRatio: 0,
				resistance: false,
				resistanceRatio: 0
            });

			$('.js-mobie-close').on('click', function () {
				$('.dot-navs').removeClass('open')
			});
			$('.js-mobie-open').on('click', function () {
				$('.dot-navs').addClass('open')
			});

		},
		playBox: function () {
			var $playBox = $('.play-box');
			$playBox.on('click', '.play-box__toggle', function () {
				var _playBox = $(this).parents('.play-box');
				var _playBoxSrc = _playBox.find('[data-src]').data('src');

				/// ----
				// var _costr = "<iframe src='" + _playBoxSrc + "?autoplay=1' frameborder='0' allowfullscreen></iframe>";
				// _playBox.find('.play-box__flex').append(_costr);
                //
				// _playBox.find('.play-box__modal').addClass('active');

				if($(window).width() < 768) {
					window.open(_playBoxSrc+ '?autoplay=1', '_blank');
				} else {
					var _costr = "<iframe src='" + _playBoxSrc + "?autoplay=1' frameborder='0' allowfullscreen></iframe>";
					_playBox.find('.play-box__flex').append(_costr);

					_playBox.find('.play-box__modal').addClass('active');
				}
			});

			$playBox.on('click', '.play-box__close', function () {
				var _playBox = $(this).parents('.play-box');
				_playBox.find('iframe').remove();
				_playBox.find('.play-box__modal').removeClass('active');
			});
		},
		vsBall: function () {
            $('.vs-ball--type-1').each(function () {
                new Swiper($(this), {
                    autoplay: 4000,
                    speed: 800,
                    loop: true,
                    effect: 'fade',
					preventClicks: true,
					followFinger: false,
					shortSwipes: false,
					longSwipes: false
                });
            });
			$('.vs-ball--type-2').each(function () {
				new Swiper($(this), {
					autoplay: 9000,
					speed: 800,
					loop: true,
					effect: 'fade',
					preventClicks: true,
					followFinger: false,
					shortSwipes: false,
					longSwipes: false
				});
			});
			$('.vs-ball--type-3').each(function () {
				new Swiper($(this), {
					autoplay: 6000,
					speed: 800,
					loop: true,
					effect: 'fade',
					preventClicks: true,
					followFinger: false,
					shortSwipes: false,
					longSwipes: false
				});
			})
		},
		smoothScroll: function () {

			$('.dot-navs__hash, .scroll-down, .side-register').click(function() {
				$('.dot-navs__hash').parents('.dot-navs__item').removeClass('active');
				$(this).parents('.dot-navs__item').addClass('active');
				if(website.helper.isMobile()) {
					$('.dot-navs').removeClass('open')
				}
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					var target = $(this.hash);
					target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						$('html,body').animate({
							scrollTop: target.offset().top
						}, 1000);
						return false;
					}
				}
			});
		},
		sectionVisibility: function () {
			var viewScreenActive = function () {
				$('.view-screen').each(function () {
					var isInView = utils.isElementInView($(this), false);
					if(isInView) {
						$(this).addClass('active');
					} else {
						$(this).removeClass('active');
					}
				});
			};
			var masterHead = function () {
				var inView = utils.isElementInView($('#masterhead'), false);
				if(inView) {
					$('.dot-navs').removeClass('active');
					$('.scroll-down').css('display', 'block');
				} else {
					$('.dot-navs').addClass('active');
					$('.scroll-down').css('display', 'none');
				}
			};

			var masterRegister = function () {
				var masterIsVisible = $('#masterhead').hasClass('active');
				var registerIsVisible = utils.isElementInView($('#register'), false);

				if(registerIsVisible || masterIsVisible) {
					$('.side-register.mob').css('display', 'none');
				} else {
					$('.side-register.mob').css('display', 'block');
				}
			};

			var idSection = function () {
				$('.dot-navs__item').removeClass('active');
				$('.id-section').each(function () {
					var idView = utils.isElementInView($(this), false);
					if(idView) {
						$('.dot-navs__hash[href="#' + $(this).attr('id') +'"]').parents('.dot-navs__item').addClass('active');
					}

					var isRegister = idView && $(this).attr('id') == "register";

					if(isRegister && !website.helper.isMobile()) {
						$('.dot-navs .side-register').css('display', 'none');
					} else {
						$('.dot-navs .side-register').css('display', 'block');
					}
				})
			};

			masterHead();
			if(website.helper.isMobile()) {
				$(window).scroll(masterRegister);
			}
			$(window).scroll(idSection);
			$(window).scroll(masterHead);
			$(window).scroll(viewScreenActive);

		}
	};

	return {
		run: function () {
			page.init();
		}
	};

}());

$(document)
	.foundation()
	.ready(function () {
		website.core.run();
	});
